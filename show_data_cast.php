<?php
    $DB_NAME = "uas_android";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $id_movie = $_POST['id_movie'];
        $sql = "SELECT m.id_movie, c.id_celeb, c.nama_celeb, g.nama_cast, concat('http://192.168.43.157/uas_android/cover/',foto_celeb) as url
        FROM movie m, celeb c, grx g
        WHERE m.id_movie = g.id_movie AND c.id_celeb = g.id_celeb and m.id_movie = '$id_movie'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_cast = array();
            while($cast = mysqli_fetch_assoc($result)){
                array_push($data_cast, $cast);
            }
            echo json_encode($data_cast);
        }
    }
?>