<?php
    $DB_NAME = "uas_android";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $mode = $_POST['mode'];
        $respon = array();
        $respon['kode'] = '000';

        switch($mode){
            case "insert":
                $nama_movie = $_POST['nama_movie'];
                $tahun_movie = $_POST['tahun_movie'];
                $sinopsis_movie = $_POST['sinopsis_movie'];
                $nama_genre = $_POST['nama_genre'];
                $imstr = $_POST['image'];
                $file = $_POST['file'];
                $path = "cover/";
                $trailer = "blackwidow.mp4";

                $sql = "select id_genre from genre where nama_genre='$nama_genre'";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $id_genre = $data['id_genre'];

                    $sql = "insert into movie(nama_movie, tahun_movie, sinopsis_movie, id_genre, cover_movie, trailer_movie) values('$nama_movie', '$tahun_movie', '$sinopsis_movie', '$id_genre', '$file', '$trailer')";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        if(file_put_contents($path.$file, base64_decode($imstr)) == false){
                            $sql = "delete from movie where nama_movie='$nama_movie'";
                            mysqli_query($conn,$sql);
                            $respon['kode'] = "111";
                            echo json_encode($respon);
                            exit();
                        }
                        else{
                            echo json_encode($respon);
                            exit();
                        }
                    }
                    else{
                        $respon['kode'] = "111";
                        echo json_encode($respon);
                        exit();
                    }
                }
            break;

            case "update":
                $id_movie = $_POST['id_movie'];
                $nama_movie = $_POST['nama_movie'];
                $tahun_movie = $_POST['tahun_movie'];
                $sinopsis_movie = $_POST['sinopsis_movie'];
                $nama_genre = $_POST['nama_genre'];
                $imstr = $_POST['image'];
                $file = $_POST['file'];
                $path = "cover/";

                $sql = "select id_genre from genre where nama_genre='$nama_genre'";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $id_genre = $data['id_genre'];

                    $sql = "";
                    if($imstr == ""){
                        $sql = "update movie set nama_movie='$nama_movie', tahun_movie='$tahun_movie', sinopsis_movie='$sinopsis_movie', id_genre='$id_genre' where id_movie='$id_movie'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon);
                            exit();
                        }
                        else{
                            $respon['kode'] = "111";
                            echo json_encode($respon);
                            exit();
                        }
                    }
                    else{
                            if(file_put_contents($path.$file, base64_decode($imstr)) == false){
                                $respon['kode'] = "111";
                                echo json_encode($respon);
                                exit();
                            }
                            else{
                                $sql = "update movie set nama_movie='$nama_movie', tahun_movie='$tahun_movie', sinopsis_movie='$sinopsis_movie',  id_genre='$id_genre', cover_movie='$file' where id_movie='$id_movie'";
                                $result = mysqli_query($conn,$sql);
                                if($result){
                                    echo json_encode($respon);
                                    exit();
                                }
                                else{
                                    $respon['kode'] = "111";
                                    echo json_encode($respon);
                                    exit();
                                }
                            }
                        }
                }
            break;

            case "delete":
                $id_movie = $_POST['id_movie'];
                $sql = "select cover_movie from movie where id_movie='$id_movie'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(mysqli_num_rows($result)>0){
                        $data = mysqli_fetch_assoc($result);
                        $photos = $data['cover_movie'];
                        $path = "cover/";
                        unlink($path.$photos);
                    }

                    $sql = "delete from movie where id_movie='$id_movie'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon);
                        exit();
                    }
                    else{
                        $respon['kode'] = "111";
                        echo json_encode($respon);
                        exit();
                    }
                }
            break;
        }
    }
?>