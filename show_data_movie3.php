<?php
    $DB_NAME = "uas_android";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $id_celeb = $_POST['id_celeb'];
        $sql = "SELECT m.id_movie, gr.nama_genre, m.sinopsis_movie, c.id_celeb, m.nama_movie, g.nama_cast, m.tahun_movie, concat('http://192.168.43.157/uas_android/cover/',cover_movie) as url, concat('http://192.168.43.157/uas_android/video/',trailer_movie) as url2
                FROM movie m, celeb c, grx g, genre gr
                WHERE m.id_movie = g.id_movie AND c.id_celeb = g.id_celeb and m.id_genre=gr.id_genre and c.id_celeb = '$id_celeb'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_movie = array();
            while($movie = mysqli_fetch_assoc($result)){
                array_push($data_movie, $movie);
            }
            echo json_encode($data_movie);
        }
    }
?>