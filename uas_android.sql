-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Bulan Mei 2020 pada 14.53
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uas_android`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `celeb`
--

CREATE TABLE `celeb` (
  `id_celeb` int(11) NOT NULL,
  `nama_celeb` varchar(50) NOT NULL,
  `foto_celeb` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `celeb`
--

INSERT INTO `celeb` (`id_celeb`, `nama_celeb`, `foto_celeb`) VALUES
(1, 'Scarlett Johansson', 'scarlett_johansson.jpg'),
(2, 'Florence Pugh', 'florence_pugh.jpg'),
(3, 'Robert Downey Jr', 'robert_downey.jpg'),
(8, 'Chris Evan', 'DC20200518194713.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `genre`
--

CREATE TABLE `genre` (
  `id_genre` int(11) NOT NULL,
  `nama_genre` varchar(15) NOT NULL,
  `foto_genre` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `genre`
--

INSERT INTO `genre` (`id_genre`, `nama_genre`, `foto_genre`) VALUES
(1, 'Action', 'action.PNG'),
(2, 'Adventure', 'adventure.PNG'),
(3, 'Animation', 'animation.PNG'),
(4, 'Comedy', 'comedy.PNG'),
(5, 'Crime', 'crime.PNG'),
(6, 'Drama', 'drama.PNG'),
(7, 'Fantasy', 'fantasy.PNG'),
(8, 'Horror', 'horror.PNG'),
(9, 'Mystery', 'mystery.PNG'),
(10, 'Romance', 'romance.PNG'),
(11, 'Sci - Fi', 'sci-fi.PNG'),
(12, 'Thriller', 'thriller.PNG');

-- --------------------------------------------------------

--
-- Struktur dari tabel `grx`
--

CREATE TABLE `grx` (
  `id_movie` int(11) NOT NULL,
  `id_celeb` int(11) NOT NULL,
  `nama_cast` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `grx`
--

INSERT INTO `grx` (`id_movie`, `id_celeb`, `nama_cast`) VALUES
(1, 2, 'Ahmad'),
(1, 1, 'Natasha'),
(4, 1, 'Jalaludin'),
(4, 2, 'Lala'),
(6, 2, 'Eni'),
(1, 3, 'Bambang'),
(10, 1, 'Tresno'),
(11, 2, 'Happy'),
(13, 1, 'Jokowi'),
(15, 8, 'Captain America'),
(15, 1, 'Natasha Romannoff');

-- --------------------------------------------------------

--
-- Struktur dari tabel `movie`
--

CREATE TABLE `movie` (
  `id_movie` int(11) NOT NULL,
  `nama_movie` varchar(100) DEFAULT NULL,
  `tahun_movie` varchar(5) NOT NULL,
  `sinopsis_movie` varchar(250) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `cover_movie` varchar(100) DEFAULT NULL,
  `trailer_movie` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `movie`
--

INSERT INTO `movie` (`id_movie`, `nama_movie`, `tahun_movie`, `sinopsis_movie`, `id_genre`, `cover_movie`, `trailer_movie`) VALUES
(1, 'Black Widow', '2020', 'A film about Natasha Romanoff in her quests between the films Civil War and Infinity War.', 1, 'blackwidow.jpg', 'blackwidow.mp4'),
(4, 'Itaewon Class', '2020', 'The story of Park Sae Ro Yi who opens a restaurant in Itaewon.', 1, 'itaewonclass.jpg', 'itaewonclass.mp4'),
(6, 'Birds Of Prey', '2020', 'After splitting with the Joker, Harley Quinn joins superheroes Black Canary, Huntress and Renee Montoya to save a young girl from an evil crime lord.v vvv:vvvvvvvvvfcccccc', 5, 'birdsofprey.jpg', 'birdsofprey.mp4'),
(15, 'Avengers End Game', '2020', 'Film Avengers Tentang Super Hero', 1, 'DC20200518194607.jpg', 'blackwidow.mp4');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `celeb`
--
ALTER TABLE `celeb`
  ADD PRIMARY KEY (`id_celeb`);

--
-- Indeks untuk tabel `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id_genre`);

--
-- Indeks untuk tabel `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id_movie`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `celeb`
--
ALTER TABLE `celeb`
  MODIFY `id_celeb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `genre`
--
ALTER TABLE `genre`
  MODIFY `id_genre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `movie`
--
ALTER TABLE `movie`
  MODIFY `id_movie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
